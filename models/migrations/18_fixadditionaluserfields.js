/* jshint esversion: 8 */
const Sequelize = require('sequelize');

module.exports = {
	up: (query) => {
	return query.addColumn('user', 'address_addon', {type:  Sequelize.STRING,	allowNull: true})
		.then(() => query.addColumn('user', 'other_account_owner', {type:  Sequelize.BOOLEAN,	allowNull: true}) )
	},
	down: async (query) => {
	    return query.dropColumn('user', 'address_addon')
	    .then(() => query.dropColumn('user', 'other_account_owner'))
	}
}