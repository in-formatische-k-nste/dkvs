/* jshint esversion: 8 */
const Sequelize = require('sequelize');

module.exports = {
	up: (query) => {
	return query.addColumn('user', 'other_account_owner_email', {type:  Sequelize.STRING,	allowNull: true})
	},
	down: async (query) => {
	    return query.dropColumn('user', 'other_account_owner_email')
	}
}